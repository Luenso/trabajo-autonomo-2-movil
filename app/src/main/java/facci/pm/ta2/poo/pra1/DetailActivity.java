package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {
    TextView   tvnombre, tvprecio, tvdescripcion;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        tvnombre = findViewById(R.id.nombre);
        tvprecio = findViewById(R.id.precio);
        tvdescripcion = findViewById(R.id.descripcion);
        image = findViewById(R.id.thumbnail);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        //Se recibe el parametro de la activity anterior
        String object_id = getIntent().getStringExtra("object_id");
        //Se crea el objeto de la clase DataQuery y se asigna el valor de item
        DataQuery query = DataQuery.get("item");
        //Se genera el metodo getInBackground que permitirá asignar los datos
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null ){
                    //Se crean las variables para captar los valores
                    String nombre = (String) object.get("name");
                    String precio = (String) object.get("price");
                    String descripcion = (String) object.get("description");
                    Bitmap bitmap = (Bitmap) object.get("image");
                    //Se asignan los valores a los textview correspondientes
                    tvnombre.setText(nombre+"$");
                    tvprecio.setText(precio);
                    tvdescripcion.setText(descripcion);
                    image.setImageBitmap(bitmap);
                }
            }
        });


        // FIN - CODE6

    }

}


